<?php

/**
 * Implements hook_drush_command().
 */

function drush_adds_drush_command() {
	$items = array();
  $items['reset-schema-version'] = array(
    'description' => "Reset a module's database schema version.",
    'arguments' => array(
      'name' => 'The name of the module.',
    ),
    'options' => array(
      'value' => '',
    ),
    'callback' => 'drush_reset_schema_version',
    //'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'examples' => array(
      'drush rsv block' => '',
      'drush rsv --value=7110 block' => '',
    ),
    'aliases' => array('rsv'),
  );

	$items['migrate-import-multiple-group'] = array(
	    'description' => '',
	    'arguments' => array(
	      'migration' => 'Goup Name(s) of migration(s) to import.',
	    ),
	    'callback' => 'drush_migrate_import_multiple',
	    'aliases' => array('migroups'),
	);
  
	$items['migrate-rollback-multiple-group'] = array(
	    'description' => '',
	    'arguments' => array(
	      'migration' => 'Goup Name(s) of migration(s) to import.',
	    ),
	    'callback' => 'drush_migrate_rollback_multiple',
	    'aliases' => array('mrgroups'),
	);
	
	return $items;
}

/**
 * Callback for the migroups command
 */

function drush_migrate_import_multiple() {
	try {
	   	$args = func_get_args();
	   	foreach ($args as $group) {
	   		$migration_objects = migrate_migrations();
	   		foreach ($migration_objects as $name => $migration) {
			    if (drupal_strtolower($group) !=
			        drupal_strtolower($migration->getGroup()->getName()) ||
			            !$migration->getEnabled()) {
			    unset($migration_objects[$name]);
				}
    		};
    		if(sizeof($migration_objects) == 0) {
    			print_r(dt("Migration group !group_name does not exist \n", 
    				array('!group_name' => $group)));
    		}
    		else {
    			print_r(dt("Importing group !group_name \n",array('!group_name' => $group)));
    			_process_migrate_import_multiple($migration_objects);
    		}    		
	   	}   	
    }
  	catch (MigrateException $e) {
    drush_print($e->getMessage());
    exit;
  }
}

/**
 * Callback for the mrgroups command
 */
function drush_migrate_rollback_multiple() {
	try {
	   	$args = func_get_args();
	   	foreach ($args as $group) {
	   		$migration_objects = migrate_migrations();
	   		// Rollback in reverse order
    		$migration_objects = array_reverse($migration_objects, TRUE);
	   		foreach ($migration_objects as $name => $migration) {
			    if (drupal_strtolower($group) !=
			        drupal_strtolower($migration->getGroup()->getName()) ||
			            !$migration->getEnabled()) {
			    unset($migration_objects[$name]);
				}
    		};
    		if(sizeof($migration_objects) == 0) {
    			print_r(dt("Migration group !group_name does not exist \n", 
    				array('!group_name' => $group)));
    		}
    		else {
    			print_r(dt("Rolling back group !group_name \n",array('!group_name' => $group)));
    			_process_migrate_rollback_multiple($migration_objects);
    		}    		
	   	}   	
    }
  	catch (MigrateException $e) {
    drush_print($e->getMessage());
    exit;
  }
}

/**
 * Helper function,
 * Process Import for all items of $migrations
 */

function _process_migrate_import_multiple($migrations) {
	 foreach ($migrations as $machine_name => $migration) {
      drush_log(dt("Importing '!description' migration",
        array('!description' => $machine_name)));
      if (drush_get_option('update')) {
        $migration->prepareUpdate();
      }
      if (drush_get_option('needs-update')) {
        $map_rows = $migration->getMap()->getRowsNeedingUpdate(10000);
        $idlist = array();
        foreach ($map_rows as $row) {
          $idlist[] = $row->sourceid1;
        }
        $options['idlist'] = implode(',', $idlist);
      }
      $options['force'] = TRUE;
      // The goal here is to do one migration in the parent process and then
      // spawn subshells as needed when memory is depleted. We show feedback
      // after each subshell depletes itself. Best we can do in PHP.
      if (!drush_get_context('DRUSH_BACKEND')) {
        // Our first pass and in the parent process. Run a migration right here.
        $status = $migration->processImport($options);
        if ($status == MigrationBase::RESULT_SKIPPED) {
          drush_log(dt("Skipping migration !name due to unfulfilled dependencies:\n  !depends\nUse the --force option to run it anyway.",
            array(
              '!name' => $machine_name,
              '!depends' => implode("\n  ", $migration->incompleteDependencies()),
            )),
            'warning');
        }
        elseif ($status == MigrationBase::RESULT_STOPPED) {
          break;
        }
        elseif ($status == MigrationBase::RESULT_INCOMPLETE) {
          $stop = TRUE;
        }

        // Subsequent run in the parent process. Spawn subshells ad infinitum.
        $migration_string = implode(',', array_keys($migrations));
        while ($status == MigrationBase::RESULT_INCOMPLETE) {
          $return = drush_migrate_invoke_process($migration_string);
          // 'object' holds the return code we care about.
          $status = $return['object']['status'];
          $migration_string = $return['object']['migrations'];
          if ($status == MigrationBase::RESULT_SKIPPED) {
            drush_log(dt("Skipping migration !name due to unfulfilled dependencies:\n  !depends\nUse the --force option to run it anyway.",
              array(
                '!name' => $machine_name,
                '!depends' => implode("\n  ", $migration->incompleteDependencies()),
              )),
              'warning');
          }
          elseif ($status == MigrationBase::RESULT_STOPPED) {
            $stop = TRUE;
            break;
          }
        }
      }
      else {
        // I'm in a subshell. Import then set return value so parent process can respawn or move on.
        $status = $migration->processImport($options);
        if ($status == MigrationBase::RESULT_SKIPPED) {
          drush_log(dt("Skipping migration !name due to unfulfilled dependencies:\n  !depends\n",
            array(
              '!name' => $machine_name,
              '!depends' => implode("\n  ", $migration->incompleteDependencies()),
            )),
            'warning');
        }
        elseif ($status == MigrationBase::RESULT_INCOMPLETE) {
          $stop = TRUE;
        }
        drush_backend_set_result(array('status' => $status,
          'migrations' => implode(',', array_keys($migrations))));
      }
      if ($stop) {
        break;
      }
      unset($migrations[$machine_name]);
    }
}
/**
 * Helper function,
 * Process rollback for all items of $migrations
 */
function _process_migrate_rollback_multiple($migrations) {

	if (drush_get_option('force', FALSE) == 1) {
      $options['force'] = TRUE;
    }
    foreach ($migrations as $migration) {
      drush_log(dt("Rolling back '!description' migration",
        array('!description' => $migration->getMachineName())));
      $return = $migration->processRollback($options);
      // If it couldn't finish (presumably because it was appraoching memory_limit),
      // continue in a subprocess
      if ($return == MigrationBase::RESULT_INCOMPLETE) {
        drush_migrate_invoke_process();
      }
      // If stopped, don't process any further
      elseif ($return == MigrationBase::RESULT_STOPPED) {
        break;
      }
      elseif ($return == MigrationBase::RESULT_SKIPPED) {
        drush_log(dt("Skipping migration !name due to unfulfilled dependencies, use the --force option to run it anyway.",
          array('!name' => $migration->getMachineName())),
          'warning');
      }
    }
}


function drush_reset_schema_version() {
  $args = func_get_args();
    if (!isset($args[0])) {
      drush_set_error('DRUSH_SCHEMA_ERROR', dt('No module specified.'));
    }
    else {
      $value = drush_get_option('value');
      if(!$value) {
        $value = 0;
      }
      //TODO: rewrite this following query using db_update()
      $result = db_query(
        "UPDATE  {system} 
         SET  schema_version = :schema_version 
         WHERE name = :module_name", 
        array(':schema_version' => $value, 'module_name' => $args[0]));

       drush_log(dt('!module_name\'s schema version was set to !value', 
        array('!module_name' => $args[0], '!value' => $value)), 'success');
      return '';
    }
}